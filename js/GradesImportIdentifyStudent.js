/**
 * Identify Student by ID or Username or Name JS
 *
 * @package Grades Import module
 */

var gradesImportIdentifyStudent = function( withId ) {
	var withIds = ['STUDENT_ID', 'USERNAME', 'NAME'];

	for ( max = withIds.length, i = 0; i < max; i++ ) {
		if ( withIds[ i ] === withId ) {
			$( '#' + withIds[ i ] ).show().css('position', 'relative').css('top', 'auto');

			// Enable select & update chosen.
			$( '#' + withIds[ i ] ).find('select').prop('disabled', false).trigger("chosen:updated");

			continue;
		}

		$( '#' + withIds[ i ] ).hide();

		// Disable select & update chosen.
		$( '#' + withIds[ i ] ).find('select').prop('disabled', true).trigger("chosen:updated");
	}
};

$('.onchange-student-identify-select').on('change', function() {
	gradesImportIdentifyStudent(this.value);
});
