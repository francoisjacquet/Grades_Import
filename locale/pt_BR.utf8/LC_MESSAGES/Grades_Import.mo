��          �      ,      �     �     �  *   �     �     �     �               6     D  @   U     �  
   �     �     �  '   �  �  �     �  	   �  5   �     #     C     \     k     }     �     �  8   �     �           $     E  '   K                              	                                         
       %s grades were imported. %s rows Are you absolutely ready to import grades? Cannot open file. Gradebook Grades Fields Grades Import Identify Student Import Gradebook Grades Import Grades Import first row No Assignments were found for current Course Period and Quarter. No student found. Reset form Select CSV or Excel file Stop Student not scheduled in course Period. Project-Id-Version: Grades Import Module for RosarioSIS.
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 17:48+0200
Last-Translator: Emerson Barros
Language-Team: RosarioSIS <info@rosariosis.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 %s notas foram importadas. %s linhas Você está absolutamente pronto para importar notas? O arquivo não pode ser aberto. Campos de notas do livro Importar notas Identificar Aluno Importar notas do livro Importar notas Importar primeira linha Nenhuma tarefa foi encontrada para a turma ou trimestre. Nenhum aluno encontrado. Redefinir formulário Selecione o arquivo CSV ou Excel Parar O aluno não está agendado nesta aula. 