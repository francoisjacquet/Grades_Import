��          �      ,      �     �     �  *   �     �     �     �               6     D  @   U     �     �  
   �     �     �  �  �     �  
   �  ,   �          7  	   P     Z     m     �     �  C   �     �     �          +     K                             	                                         
        %s grades were imported. %s rows Are you absolutely ready to import grades? Cannot open file. Gradebook Grades Fields Grades Import Identify Student Import Gradebook Grades Import Grades Import first row No Assignments were found for current Course Period and Quarter. No points found. No student found. Reset form Select CSV or Excel file Stop Project-Id-Version: Grades Import Module for RosarioSIS.
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 17:49+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 %s ocene so bile uvožene. %s vrstice Ali ste popolnoma pripravljeni na uvoz ocen? Datoteke ni mogoče odpreti. Polja ocen v redovalnici Uvoz ocen Prepoznajte dijaka Uvozi ocene iz redovalnice Uvozi ocene Uvozi prvo vrstico Za trenutno obdobje vzgojne skupine ni bilo najdenih nobenih nalog. Ni najdenih točk. Najden ni bil noben dijak. Ponastavi obrazec Izberite datoteko CSV ali Excel Ustavi 